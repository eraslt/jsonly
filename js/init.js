
var canvas = document.getElementById("canvas");
var center = {x:Math.floor(canvas.width/2), y:Math.floor(canvas.height/2)}
var ctx = canvas.getContext("2d");

var max_map_size = 100;


Sprites.load('assets/tilesets/pimp.png', 'char', 32, 32);
Sprites.load('assets/tilesets/rat-dog.png', 'rat_dog', 32, 32);
Sprites.load('assets/tilesets/rat-dog-grey.png', 'rat_dog-grey', 32, 32);
Sprites.load('assets/tilesets/rat-dog-layer-1.png', 'rat_dog-layer-1', 32, 32);
Sprites.load('assets/tiles1.png', 'tileset', 32, 32);
Sprites.load('assets/tilesets/grass.png', 'grass', 8, 8);
Sprites.load('assets/tilesets/flowers.png', 'flowers', 8, 8);
Sprites.load('assets/tilesets/tree3.png', 'trees', 32, 64);


var Player = new Entity(Math.floor(max_map_size/2), Math.floor(max_map_size/2));

//Player.health = 100;

Player.load("{'x':50,'y':50,'sprite':0,'health':200}");
Player.name = "Player";
//Player.load(Player.save());


function fixCoord(coord, min, max) {
    return coord>max?max:coord<min?min:coord;
}

