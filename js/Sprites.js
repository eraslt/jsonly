var Sprites = {
    d : [],

    draw : function(ctx, x, y, group, index) {

        if (typeof(Sprites.d[group])=="undefined") {
            return false;
        }

        var sprite = Sprites.d[group];

        if (index>sprite.totalTiles) {
            return false;
        }

        var j = Math.floor(index/sprite.tilesX);
        var i = Math.floor(index - j*sprite.tilesX);
        //TODO kai didels drawint i wirsu gi
        ctx.drawImage(sprite.img, i*sprite.tileWidth, j*sprite.tileHeight, sprite.tileWidth, sprite.tileHeight, x, y-sprite.tileHeight+32, sprite.tileWidth, sprite.tileHeight);
        return true;
    },

    load : function(src, group, tileWidth, tileHeight) {
        var image = new Image();
            image.src = src;
            image.onload = function() {
                Sprites.create(image, group, tileWidth, tileHeight);
            };
    },

    create : function(image, group, tileWidth, tileHeight) {

        var tilesX = image.width / tileWidth;
        var tilesY = image.height / tileHeight;
        var totalTiles = tilesX * tilesY;

        Sprites.d[group] = {
                img : image,
                tileWidth : tileWidth,
                tileHeight : tileHeight,
                tilesX : tilesX,
                tilesY : tilesY,
                totalTiles : totalTiles
        };
        return totalTiles;
    }
};
