
var SomePathFinder = {

    findDistances : function(map, pos) {
        for(var x=0; x<map.length; x++){
            for(var y=0; y<map.length; y++){
                map[x][y] = this.euclideanDistance({x:x, y:y}, pos);
            }
        }
        return map;
    },

    simpleWalker : function(start, end) {
        return simpleWalker(start, end, true);
    },

    simpleWalker : function(start, end, diagonal) {
        var max_dist = this.manhattanDistance(start, end);
        var path = new Array();
        start.weight = this.manhattanDistance(start, end);
        var cnt = 0;
        while (JSON.stringify(start)!=JSON.stringify(end)&&++cnt<=max_dist) {
            list = this.findNeighbors(start, diagonal);
            path.push(start);
            for (var a in list) {
                list[a].weight = this.manhattanDistance(list[a], end);
                if (list[a].weight < start.weight) {
                    start = list[a];
                }
            }
        }
        path.push(end);
        return path;
    },

    manhattanDistance : function(a, b) {
        return Math.floor(Math.abs(a.x - b.x) + Math.abs(a.y - b.y));
    },

    euclideanDistance : function(a, b) {
        return Math.sqr((b.x - a.x) * (b.x - a.x) + (b.y - a.y) *(b.y - a.y)); 
    },

    findNeighbors : function(node) {
        return findNeighbors(node, true);
    },

    findNeighbors : function(node, diagonal) {
        list = new Array();
        for(var i=-1; i<2; i++){
            for(var j=-1; j<2; j++){
                if(i == 0 && j == 0) continue;
                if (!diagonal && Math.abs(i+j)!=1) continue;
                list.push({x:node.x + i, y:node.y + j});
            }
        }
        return list;
    },

    test : function() {
        var w = 100;
        var path = SomePathFinder.simpleWalker(
                {x:Math.floor((Math.random() * w) + 1), y:Math.floor((Math.random() * w) + 1)},
                {x:Math.floor((Math.random() * w) + 1), y:Math.floor((Math.random() * w) + 1)}
        );
        console.log(path);
    }
};
