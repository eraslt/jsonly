var CellularAutomata = {
    
    chanceToStartAlive : 0.2,
    deathLimit : 2,
    birthLimit : 4,
    random : null,

    initialiseMap: function(map) {
        for(var x=0; x<map.length; x++) {
            for(var y=0; y<map.length; y++) {
                if(CellularAutomata.random() < this.chanceToStartAlive) {
                    map[x][y] = true;
                }
            }
        }
        return map;
    },

    /**
     * Generates boolean map[100][100] using celular algorytm for n iterrations
     * @param int numberOfSteps
     * @return boolean[][]
     */
    generateMap: function(numberOfSteps) {
        var cellmap = new Array(100);
        for (var i = 0; i < 100; i++) {
            cellmap[i] = new Array(100);
        }
        return this.generateMapFromMap(numberOfSteps, cellmap);
    },

    /**
     * Generates boolean map from provided map, using celular algorytm for n iterrations
     * @param int numberOfSteps
     * @param boolean[][] cellmap
     * @return boolean[][]
     */
    generateMapFromMap: function(numberOfSteps, cellmap) {
        //Set up the map with random values
        cellmap = this.initialiseMap(cellmap);
        //And now run the simulation for a set number of steps
        for(var i=0; i<numberOfSteps; i++){
            cellmap = this.doSimulationStep(cellmap);
        }
        return cellmap;
    },

    doSimulationStep: function(oldMap) {
        var newMap = new Array(oldMap.length);
        for (var i = 0; i < oldMap.length; i++) {
            newMap[i] = new Array(oldMap.length);
        }
        //Loop over each row and column of the map
        for(var x=0; x<oldMap.length; x++){
            for(var y=0; y<oldMap[0].length; y++){
                var nbs = this.countAliveNeighbours(oldMap, x, y);
                //The new value is based on our simulation rules
                //First, if a cell is alive but has too few neighbours, kill it.
                if(oldMap[x][y]){
                    if(nbs < this.deathLimit){
                        newMap[x][y] = false;
                    }
                    else{
                        newMap[x][y] = true;
                    }
                } //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                else{
                    if(nbs > this.birthLimit){
                        newMap[x][y] = true;
                    }
                    else{
                        newMap[x][y] = false;
                    }
                }
            }
        }
        return newMap;
    },

    /**
     * Returns the number of cells in a ring around (x,y) that are alive.
     * @param boolean[][] map
     * @param int x
     * @param int y
     * @return int
     */
    countAliveNeighbours: function(map, x, y){
        var count = 0;
        for(var i=-1; i<2; i++){
            for(var j=-1; j<2; j++){
                var neighbour_x = x+i;
                var neighbour_y = y+j;
                //If we're looking at the middle point
                if(i == 0 && j == 0){
                    //Do nothing, we don't want to add ourselves in!
                }
                //In case the index we're looking at it off the edge of the map
                else if(neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= map.length || neighbour_y >= map[0].length){
                    count = count + 1;
                }
                //Otherwise, a normal check of the neighbour
                else if(map[neighbour_x][neighbour_y]){
                    count = count + 1;
                }
            }
        }
        return count;
    }
}
