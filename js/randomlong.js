var randomLong_x = 0;


function randomLong() {
    if (randomLong_x==0) {
        if (world_seed!=undefined && world_seed>0) {
            console.log('a');
            randomLong_x = world_seed;
        } else {
            console.log('b');
            randomLong_x = Math.random();
        }
    }

    if (randomLong_x<1 && randomLong_x>0) {
        randomLong_x = randomLong_x*Math.pow(10, (randomLong_x+'').length-2);
    }

    var x = randomLong_x;
    x ^= (x << 21);
    x ^= (x >>> 35);
    x ^= (x << 4);
    randomLong_x = x;
    return parseFloat('0.'+Math.abs(x));
}
