function Entity(x, y) {
    this.x = x;
    this.y = y;
    this.sprite = 0;

    this.setPos = function(x, y) {
        this.x = x;
        this.y = y;
    },

    this.isAlive = function() {
        return true;
    },

    this.update = function() {
        if (null != this.brain) {
            this.brain.act(this);
        }
    }
    
    this.move = function(x, y) {
        if (WorldMap.getMapSprite(x, y)) {
            return false;
        }
        if (!(tile = WorldMap.getMapTile(x, y)) || tile.tex<0) {
            return false;
        }
        this.x = x;
        this.y = y;
        return true;
    },

    this.moveLeft = function() {
        this.sprite = 3;
        return this.move(this.x - 1, this.y);
    },

    this.moveRight = function() {
        this.sprite = 1;
        return this.move(this.x + 1, this.y);
    },

    this.moveDown = function() {
        this.sprite = 0;
        return this.move(this.x, this.y - 1);
    },

    this.moveUp = function() {
        this.sprite = 2;
        return this.move(this.x, this.y + 1);
    },

    this.save = function() {
        var data_str = [];
        for (var i in this) {
            if (typeof this[i] == "string" ||
                typeof this[i] == "number" ||
                typeof this[i] == "boolean") {
                data_str.push("'"+i+"':"+this[i]);
            }
        }
        return "{"+data_str.join()+"}";
    },

    this.load = function(string) {
        var data;
        try {
            eval("data = " + string);
        } catch (e) {
            console.log(e)
        }
        if (data) {
            for (var i in data) {
                if (typeof data[i] == "string" ||
                    typeof data[i] == "number" ||
                    typeof data[i] == "boolean") {
                    this[i] = data[i];
                }
            }
        }
    }
};

