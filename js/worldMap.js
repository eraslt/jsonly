/*
 * where all numbers are integers, a and b are big primes so that the integer multiplications overflow, 
 * and c and d are some random integers. ^ is bitwise exclusive or. The result is a random integer which
 * you can scale to the desired range.
 */
function getSeed(x, z) {
    var a = 1300309;
    var b = 8269;
    var c = 15;
    var d = 1789;
    return (a * x + b * z + c) ^ d
}

function getRandomInt(min, max, random) {
    return Math.floor(random * (max - min + 1)) + min;;
}

var WorldMap  = {
    mapTitles : null,
    generate: function(w, road_count, random) {
        this.mapTitles = new Array(w);
        for (var i = 0; i < w; i++) {
            this.mapTitles[i] = new Array(w);
        }

        var wmap = new Array(w);
        for (var i = 0; i < w; i++) {
            wmap[i] = new Array(w);
        }

        CellularAutomata.random = random;
        var heightmap = this.getHeightMap(CellularAutomata.generateMapFromMap(5, wmap));

        for(var y=w-1; y>=0; y--) {
            for(var x=0; x<w; x++) {
                var m =  {
                    is_road : false,
                    dir : null,
                    texture : 1,
                    blocks : 0,
                    decoration : null,
                    sprite : null
                };

                if (heightmap[x][y]>0) {
                    m.blocks = heightmap[x][y];
                    m.texture = 0;
                }

                if (random()<0.3) {
                    m.decoration = {tex : 0, ox : 0, oy : 0};
                    m.decoration.tex = getRandomInt(0, 14, random());
                    m.decoration.ox = getRandomInt(0, 24, random());
                    m.decoration.oy = getRandomInt(0, 24, random());
                }

                if (random()<0.2) {
                    m.sprite = getRandomInt(0, 1, random());
                }

                this.mapTitles[x][y] = m;
            }
        }
    },

    getHeightMap: function(w) {
        var map = new Array(w.length);
        for (var i = 0; i < w.length; i++) {
            map[i] = new Array(w.length);
        }
        for(var x=0; x<w.length; x++) {
            for(var y=0; y<w.length; y++) {
                if (w[x][y]) {
                    map[x][y] = 0;
                } else {
                    for(var i=-1; i<2; i++){
                        for(var j=-1; j<2; j++){
                            if(i == 0 && j == 0) continue;
                            if (Math.abs(i+j)!=1) continue;
                            if (x + i < 0 || y + j < 0) continue;
                            if (x + i > w.length-1 || y + j > w.length-1) continue;
                            if (map[x][y] == null) {
                                map[x][y] = 0;
                            }
                            map[x][y] += w[x+i][y+j]?0:1;
                        }
                    }
                }
            }
        }
        return map;
    },

    getCollisionMap: function() {
        var len = this.mapTitles.length;
        var coll = new Array(len);
        for(var x = 0; x < len; x++) {
            coll[x] = new Array(len);
            for(var y = 0; y < len; y++) {
                coll[x][y] = 1;
                if (this.mapTitles[x] && this.mapTitles[x][y]) {
                    if (this.mapTitles[x][y].sprite) {
                        coll[x][y] = 0;
                    }
                }
            }
        }
        return coll;
    },

    getMapTile: function(x, y) {
        if (!this.mapTitles[x] || !this.mapTitles[x][y]) {
            return false;
        }
        return this.mapTitles[x][y];
    },

    getMapDecoration : function (x, y) {
        if (!this.mapTitles[x]) return false;
        if (!this.mapTitles[x][y]) return false;
        if (!this.mapTitles[x][y].decoration) return false;
        return this.mapTitles[x][y].decoration;
    },
    
    getMapSprite : function (x, y) {
        if (!this.mapTitles[x]) return false;
        if (!this.mapTitles[x][y]) return false;
        if (!this.mapTitles[x][y].sprite) return false;
        return this.mapTitles[x][y].sprite;
    }
}
