function IsPlayerInRange() {

    var r = new IsInRange();
    for (var i in r) {
        this[i] = r[i];
    }

    this.act = function(entity) {
        var a = Math.abs(entity.x-Player.x)+Math.abs(entity.y-Player.y);
        if (this.isInRange(Player, entity)) {
            console.log("player is in "+a+" range for:"+entity.name);
            this.succeed();
            entity.target = Player;
            return;
        }
        this.fail();
    }
}

