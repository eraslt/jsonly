function IsWeaker() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.act = function(entity) {
        if (!entity.target) {
            this.fail();
            return;
        }
        if (!entity.target.health) {
            this.fail();
            return;
        }
        if (!entity.health) {
            this.fail();
            return;
        }
        if (!entity.health<=entity.target.health) {
            this.fail();
            return;
        }
        this.succeed();
    }
}

