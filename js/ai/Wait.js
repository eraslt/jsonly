function Wait(times) {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.times = 1;
    if (times && times>0) {
        this.times = times;
    }

    this.originalTimes = this.times;

    this.reset = function() {
        this.times = this.originalTimes;
    }

    this.act = function(entity) {
        if (this.times > 0) {
            this.times--;
        }
        if (this.times == 0) {
            this.succeed();
        }
    }
}
