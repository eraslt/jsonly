function RunAway() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.target = null;
    this.random = function() {return ~~(Math.random()*8)};

    this.findPath = function(x, y, x2, y2) {

        x = fixCoord(x, 0, max_map_size);
        y = fixCoord(y, 0, max_map_size);
        x2 = fixCoord(x2, 0, max_map_size);
        y2 = fixCoord(y2, 0, max_map_size);

        var graph = new Graph(WorldMap.getCollisionMap());

        try {
            return astar.search(graph, graph.grid[x][y], graph.grid[x2][y2]);
        } catch (e) {
            console.log(e);
        }
    }

    this.act = function(entity) {
        this.target = entity.target;
        var cords = {x: entity.x-entity.target.x, y: entity.x-entity.target.x};
        var path = this.findPath(entity.x, entity.y, cords.x, cords.y);
        if (path==0) {
            this.fail();
            return;
        }
        //TODO: need to finish this
        console.log("run away");
        this.succeed();
        return;

        this.moveTo = new MoveTo(path[0].x, path[0].y);
        this.moveTo.start();
        this.moveTo.act(entity);
        if (this.moveTo.isSuccess()) {
            this.succeed();
        } else if (this.moveTo.isFailure()) {
            this.fail();
        }
    }

    this.isInRange = function(x1, y1, x2, y2) {
        var range = 5;
        return (Math.abs(x1 - x2) <= range && Math.abs(y1 - y2) <= range);
    }
}
