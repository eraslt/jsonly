/**
 * this routine contains a list of one or more routines. When it acts, it
 * will succeed when one of the routines in the list succeeds. The order in
 * which the routines are executed is set by the order in which the routines
 * are passed in. If we’d like to randomise the execution of routines, it’s
 * easy to create a Random routine whose sole purpose is to randomise the
 * list of routines passed in.
 */
function Selector() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.currentRoutine = null;

    this.routines = [];
    this.index = 0;


    this.addRoutine = function(routine) {
        this.routines.push(routine);
    },

    this.reset = function() {
        for (var i in this.routines) {
            this.routines[i].reset();
        }
        this.index = 0;
        this.start();
    }

    this.start = function() {
        // start the current sequence
        this.state = RoutineState.Running;
        // reset the current queue and copy the routines from setup
        this.currentRoutine = this.routines[this.index++];
        this.currentRoutine.start();
    }

    this.act = function(entity) {
        this.currentRoutine.act(entity);
        // if is still running, then carry on
        if (this.currentRoutine.isRunning()) {
            return;
        }

        // check if the routine is successful and finish the sequence
        if (this.currentRoutine.isSuccess()) {
            this.succeed();
            return;
        }

        // We need to progress the sequence. If there are no more routines
        // then the state is the last routine's state. (Success for OR was already handled)
        if (this.index>=this.routines.length) {
            this.state = this.currentRoutine.getState();
        } else {
            this.currentRoutine = this.routines[this.index++];
            this.currentRoutine.start();
        }

    }
}
