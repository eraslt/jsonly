/**
 * the sequence routine will succeed only when all the routines that it
 * contains have succeeded. For example to attack a droid, the enemy droid
 * needs to be in range, the gun needs to be loaded and the droid needs to
 * pull the trigger. Everything in this order. So the sequence contains a
 * list of routines and acts on them until all succeed. If the gun is not
 * loaded then there is no point in pulling the trigger so the whole attack
 * is a failure.
 */
function Sequence() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.currentRoutine = null;

    this.routines = [];
    this.index = 0;

    this.addRoutine = function(routine) {
        this.routines.push(routine);
    }

    this.reset = function() {
        for (var i in this.routines) {
            this.routines[i].reset();
        }
        this.index = 0;
        this.start();
    }

    this.start = function() {
        // start the current sequence
        this.state = RoutineState.Running;
        // reset the current queue and copy the routines from setup
        this.currentRoutine = this.routines[this.index];
        this.currentRoutine.start();
    }

    this.act = function(entity) {
        this.currentRoutine.act(entity);
        // if is still running, then carry on
        if (this.currentRoutine.isRunning()) {
            return;
        }

        // We need to progress the sequence. If there are no more routines
        // then the state is the last routine's state. (Success for OR was already handled)
        if (this.index>= this.routines.length) {
            this.state = this.currentRoutine.getState();
        } else {
            this.currentRoutine = this.routines[this.index++];
            this.currentRoutine.start();
        }
    }
}
