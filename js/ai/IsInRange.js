function IsInRange() {
    
    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.reset = function() {
        this.start();
    }

    this.act = function(entity) {
        for (var entity_i = 0; entity_i < entityList.length; entity_i++) {
            if (entity!==entityList[entity_i] && this.isInRange(entityList[entity_i], entity)) {
                console.log("is in range:"+entityList[entity_i].name);
                this.succeed();
                entity.target = entityList[entity_i];
                return;
            }
        }
        this.fail();
    }

    this.isInRange = function(a, b) {
        var range = 5;
        return (Math.abs(a.x - b.x) <= range && Math.abs(a.y - b.y) <= range);
    }
}

