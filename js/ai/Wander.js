function Wander() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.random = function() {return ~~(Math.random()*100)};

    this.start = function() {
        this.state = RoutineState.Running;
        this.moveTo.start();
    }

    this.reset = function() {
        this.moveTo = new MoveTo(this.random(), this.random());
    }
    this.reset();

    this.act = function(entity) {
        if (!this.moveTo.isRunning()) {
            return;
        }
        this.moveTo.act(entity);
        if (this.moveTo.isSuccess()) {
            this.succeed();
        } else if (this.moveTo.isFailure()) {
            this.fail();
        }
    }
}
