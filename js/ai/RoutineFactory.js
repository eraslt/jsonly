var RoutineFactory = {

    Routine : function() {
        var sequence = new Sequence();
        for (i = 0; i < arguments.length; i++) {
            sequence.addRoutine(arguments[i]);
        }
        return sequence;
    },

    selector : function() {
        var selector = new Selector();
        for (i = 0; i < arguments.length; i++) {
            selector.addRoutine(arguments[i]);
        }
        return selector;
    },

    repeat : function(routine, times) {
        return new Repeat(routine, times);
    },

    wander : function(entity) {
        return new Wander(entity);
    }
}
