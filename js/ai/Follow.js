function Follow() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.target = null;
    this.random = function() {return ~~(Math.random()*8)};

    this.findPath = function(x, y, x2, y2) {
        var graph = new Graph(WorldMap.getCollisionMap());

        return astar.search(graph, graph.grid[x][y], graph.grid[x2][y2]);
    }

    this.randomizeCords = function() {
        var x = this.target.x;
        var y = this.target.y;
        switch(this.random()) {
            case 0:
                x = x - 1;
                y = y - 1;
                break;
            case 1:
                y = y - 1;
                break;
            case 2:
                x = x + 1;
                y = y - 1;
                break;
            case 3:
                x = x + 1;
                break;
            case 4:
                x = x - 1;
                break;
            case 5:
                x = x - 1;
                y = y + 1;
                break;
            case 6:
                y = y + 1;
                break;
            case 7:
                x = x + 1;
                y = y + 1;
                break;
        }
        return {x:x,y:y};
    }

    //TODO: maybe check if dist 1 then success
    this.act = function(entity) {
        this.target = entity.target;
        var cords = this.randomizeCords();
        var path = this.findPath(entity.x, entity.y, cords.x, cords.y);
        if (path==0) {
            this.fail();
            return;
        }

        this.moveTo = new MoveTo(path[0].x, path[0].y);
        this.moveTo.start();
        this.moveTo.act(entity);
        if (this.moveTo.isSuccess()) {
            this.succeed();
        } else if (this.moveTo.isFailure()) {
            this.fail();
        }
    }
}
