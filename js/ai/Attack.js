function Attack() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.reset = function() {
        this.start();
    }

    this.act = function(entity) {
        console.log(entity.name+" attacking:"+entity.target.name);
        this.succeed();
    }
}

