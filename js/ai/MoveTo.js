function MoveTo(destX, destY) {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.destX = destX;
    this.destY = destY;

    this.reset = function() {
        this.start();
    }

    this.act = function(entity) {
        if (this.isRunning()) {
            if (!entity.isAlive()) {
                this.fail(entity);
                return;
            }
            //TODO: check posibility to get there
            if (this.isAtDestination(entity)) {
                this.succeed();
                return;
            }
            if (!this.move(entity)) {
                this.fail(entity);
            }
        }
    }
// TODO this bullshit works wrong
    this.move = function(entity) {
        var can_reach = false;
        if (this.destY != entity.y) {
            if (this.destY > entity.y) {
                can_reach = entity.moveUp();
            } else {
                can_reach = entity.moveDown();
            }
        }

        if (!can_reach) {
            if (this.destX != entity.x) {
                if (this.destX > entity.x) {
                    return entity.moveRight();
                } else {
                    return entity.moveLeft()
                }
            }
        }

        return true;
    }

    this.isAtDestination = function(entity) {
        return this.destX == entity.x && destY == entity.y;
    }
}
