var RoutineState = {
    Running : 0,
    Success : 1,
    Failure : 2
};

function Routine() {

    this.state = RoutineState.Running;

    this.start = function() {
        this.state = RoutineState.Running;
    }

    this.reset = function() {
        this.start()
    };

    this.act = function(tpf){};

    this.succeed = function() {
        this.state = RoutineState.Success;
    }

    this.fail = function() {
        this.state = RoutineState.Failure;
    }

    this.isSuccess = function() {
        return this.state === RoutineState.Success;
    }

    this.isFailure = function() {
        return this.state === RoutineState.Failure;
    }

    this.isRunning = function() {
        return this.state == RoutineState.Running;
    }

    this.getState = function() {
        return this.state;
    }
}
