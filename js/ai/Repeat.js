function Repeat(routine, times) {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.routine = routine;

    this.times = -1;
    if (times && times>0) {
        this.times = times;
    }

    this.originalTimes = this.times;

    this.start = function() {
        this.state = RoutineState.Running;
        this.routine.start();
    }

    this.reset = function() {
        // reset counters
        this.times = this.originalTimes;
    }

    this.act = function(entity) {
        if (this.routine.isFailure()) {
            if (true) {// TODO remove this
                this.routine.reset();
            } else {
                this.fail();
            }
        } else if (this.routine.isSuccess()) {
            if (this.times > 0 || this.times <= -1) {
                this.times--;
                this.routine.reset();
                this.routine.start();
            }
            if (this.times == 0) {
                this.succeed();
                return;
            }
        }
        if (this.routine.isRunning()) {
            this.routine.act(entity);
        }
    }
}
