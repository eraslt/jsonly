function Attack() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.reset = function() {
        this.start();
    }

    this.act = function(entity) {
        console.log(entity.name+" attacking:"+entity.target.name);
        this.succeed();
    }
}

 function Follow() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.target = null;
    this.random = function() {return ~~(Math.random()*8)};

    this.findPath = function(x, y, x2, y2) {
        var graph = new Graph(WorldMap.getCollisionMap());

        return astar.search(graph, graph.grid[x][y], graph.grid[x2][y2]);
    }

    this.randomizeCords = function() {
        var x = this.target.x;
        var y = this.target.y;
        switch(this.random()) {
            case 0:
                x = x - 1;
                y = y - 1;
                break;
            case 1:
                y = y - 1;
                break;
            case 2:
                x = x + 1;
                y = y - 1;
                break;
            case 3:
                x = x + 1;
                break;
            case 4:
                x = x - 1;
                break;
            case 5:
                x = x - 1;
                y = y + 1;
                break;
            case 6:
                y = y + 1;
                break;
            case 7:
                x = x + 1;
                y = y + 1;
                break;
        }
        return {x:x,y:y};
    }

    //TODO: maybe check if dist 1 then success
    this.act = function(entity) {
        this.target = entity.target;
        var cords = this.randomizeCords();
        var path = this.findPath(entity.x, entity.y, cords.x, cords.y);
        if (path==0) {
            this.fail();
            return;
        }

        this.moveTo = new MoveTo(path[0].x, path[0].y);
        this.moveTo.start();
        this.moveTo.act(entity);
        if (this.moveTo.isSuccess()) {
            this.succeed();
        } else if (this.moveTo.isFailure()) {
            this.fail();
        }
    }
}
 function IsInRange() {
    
    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.reset = function() {
        this.start();
    }

    this.act = function(entity) {
        for (var entity_i = 0; entity_i < entityList.length; entity_i++) {
            if (entity!==entityList[entity_i] && this.isInRange(entityList[entity_i], entity)) {
                console.log("is in range:"+entityList[entity_i].name);
                this.succeed();
                entity.target = entityList[entity_i];
                return;
            }
        }
        this.fail();
    }

    this.isInRange = function(a, b) {
        var range = 5;
        return (Math.abs(a.x - b.x) <= range && Math.abs(a.y - b.y) <= range);
    }
}

 function IsPlayerInRange() {

    var r = new IsInRange();
    for (var i in r) {
        this[i] = r[i];
    }

    this.act = function(entity) {
        var a = Math.abs(entity.x-Player.x)+Math.abs(entity.y-Player.y);
        if (this.isInRange(Player, entity)) {
            console.log("player is in "+a+" range for:"+entity.name);
            this.succeed();
            entity.target = Player;
            return;
        }
        this.fail();
    }
}

 function IsWeaker() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.act = function(entity) {
        if (!entity.target) {
            this.fail();
            return;
        }
        if (!entity.target.health) {
            this.fail();
            return;
        }
        if (!entity.health) {
            this.fail();
            return;
        }
        if (!entity.health<=entity.target.health) {
            this.fail();
            return;
        }
        this.succeed();
    }
}

 function MoveTo(destX, destY) {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.destX = destX;
    this.destY = destY;

    this.reset = function() {
        this.start();
    }

    this.act = function(entity) {
        if (this.isRunning()) {
            if (!entity.isAlive()) {
                this.fail(entity);
                return;
            }
            //TODO: check posibility to get there
            if (this.isAtDestination(entity)) {
                this.succeed();
                return;
            }
            if (!this.move(entity)) {
                this.fail(entity);
            }
        }
    }
// TODO this bullshit works wrong
    this.move = function(entity) {
        var can_reach = false;
        if (this.destY != entity.y) {
            if (this.destY > entity.y) {
                can_reach = entity.moveUp();
            } else {
                can_reach = entity.moveDown();
            }
        }

        if (!can_reach) {
            if (this.destX != entity.x) {
                if (this.destX > entity.x) {
                    return entity.moveRight();
                } else {
                    return entity.moveLeft()
                }
            }
        }

        return true;
    }

    this.isAtDestination = function(entity) {
        return this.destX == entity.x && destY == entity.y;
    }
}
 function Repeat(routine, times) {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.routine = routine;

    this.times = -1;
    if (times && times>0) {
        this.times = times;
    }

    this.originalTimes = this.times;

    this.start = function() {
        this.state = RoutineState.Running;
        this.routine.start();
    }

    this.reset = function() {
        // reset counters
        this.times = this.originalTimes;
    }

    this.act = function(entity) {
        if (this.routine.isFailure()) {
            if (true) {// TODO remove this
                this.routine.reset();
            } else {
                this.fail();
            }
        } else if (this.routine.isSuccess()) {
            if (this.times > 0 || this.times <= -1) {
                this.times--;
                this.routine.reset();
                this.routine.start();
            }
            if (this.times == 0) {
                this.succeed();
                return;
            }
        }
        if (this.routine.isRunning()) {
            this.routine.act(entity);
        }
    }
}
 var RoutineState = {
    Running : 0,
    Success : 1,
    Failure : 2
};

function Routine() {

    this.state = RoutineState.Running;

    this.start = function() {
        this.state = RoutineState.Running;
    }

    this.reset = function() {
        this.start()
    };

    this.act = function(tpf){};

    this.succeed = function() {
        this.state = RoutineState.Success;
    }

    this.fail = function() {
        this.state = RoutineState.Failure;
    }

    this.isSuccess = function() {
        return this.state === RoutineState.Success;
    }

    this.isFailure = function() {
        return this.state === RoutineState.Failure;
    }

    this.isRunning = function() {
        return this.state == RoutineState.Running;
    }

    this.getState = function() {
        return this.state;
    }
}
 var RoutineFactory = {

    Routine : function() {
        var sequence = new Sequence();
        for (i = 0; i < arguments.length; i++) {
            sequence.addRoutine(arguments[i]);
        }
        return sequence;
    },

    selector : function() {
        var selector = new Selector();
        for (i = 0; i < arguments.length; i++) {
            selector.addRoutine(arguments[i]);
        }
        return selector;
    },

    repeat : function(routine, times) {
        return new Repeat(routine, times);
    },

    wander : function(entity) {
        return new Wander(entity);
    }
}
 function RunAway() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.target = null;
    this.random = function() {return ~~(Math.random()*8)};

    this.findPath = function(x, y, x2, y2) {

        x = fixCoord(x, 0, max_map_size);
        y = fixCoord(y, 0, max_map_size);
        x2 = fixCoord(x2, 0, max_map_size);
        y2 = fixCoord(y2, 0, max_map_size);

        var graph = new Graph(WorldMap.getCollisionMap());

        try {
            return astar.search(graph, graph.grid[x][y], graph.grid[x2][y2]);
        } catch (e) {
            console.log(e);
        }
    }

    this.act = function(entity) {
        this.target = entity.target;
        var cords = {x: entity.x-entity.target.x, y: entity.x-entity.target.x};
        var path = this.findPath(entity.x, entity.y, cords.x, cords.y);
        if (path==0) {
            this.fail();
            return;
        }
        //TODO: need to finish this
        console.log("run away");
        this.succeed();
        return;

        this.moveTo = new MoveTo(path[0].x, path[0].y);
        this.moveTo.start();
        this.moveTo.act(entity);
        if (this.moveTo.isSuccess()) {
            this.succeed();
        } else if (this.moveTo.isFailure()) {
            this.fail();
        }
    }

    this.isInRange = function(x1, y1, x2, y2) {
        var range = 5;
        return (Math.abs(x1 - x2) <= range && Math.abs(y1 - y2) <= range);
    }
}
 /**
 * this routine contains a list of one or more routines. When it acts, it
 * will succeed when one of the routines in the list succeeds. The order in
 * which the routines are executed is set by the order in which the routines
 * are passed in. If we’d like to randomise the execution of routines, it’s
 * easy to create a Random routine whose sole purpose is to randomise the
 * list of routines passed in.
 */
function Selector() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.currentRoutine = null;

    this.routines = [];
    this.index = 0;


    this.addRoutine = function(routine) {
        this.routines.push(routine);
    },

    this.reset = function() {
        for (var i in this.routines) {
            this.routines[i].reset();
        }
        this.index = 0;
        this.start();
    }

    this.start = function() {
        // start the current sequence
        this.state = RoutineState.Running;
        // reset the current queue and copy the routines from setup
        this.currentRoutine = this.routines[this.index++];
        this.currentRoutine.start();
    }

    this.act = function(entity) {
        this.currentRoutine.act(entity);
        // if is still running, then carry on
        if (this.currentRoutine.isRunning()) {
            return;
        }

        // check if the routine is successful and finish the sequence
        if (this.currentRoutine.isSuccess()) {
            this.succeed();
            return;
        }

        // We need to progress the sequence. If there are no more routines
        // then the state is the last routine's state. (Success for OR was already handled)
        if (this.index>=this.routines.length) {
            this.state = this.currentRoutine.getState();
        } else {
            this.currentRoutine = this.routines[this.index++];
            this.currentRoutine.start();
        }

    }
}
 /**
 * the sequence routine will succeed only when all the routines that it
 * contains have succeeded. For example to attack a droid, the enemy droid
 * needs to be in range, the gun needs to be loaded and the droid needs to
 * pull the trigger. Everything in this order. So the sequence contains a
 * list of routines and acts on them until all succeed. If the gun is not
 * loaded then there is no point in pulling the trigger so the whole attack
 * is a failure.
 */
function Sequence() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.currentRoutine = null;

    this.routines = [];
    this.index = 0;

    this.addRoutine = function(routine) {
        this.routines.push(routine);
    }

    this.reset = function() {
        for (var i in this.routines) {
            this.routines[i].reset();
        }
        this.index = 0;
        this.start();
    }

    this.start = function() {
        // start the current sequence
        this.state = RoutineState.Running;
        // reset the current queue and copy the routines from setup
        this.currentRoutine = this.routines[this.index];
        this.currentRoutine.start();
    }

    this.act = function(entity) {
        this.currentRoutine.act(entity);
        // if is still running, then carry on
        if (this.currentRoutine.isRunning()) {
            return;
        }

        // We need to progress the sequence. If there are no more routines
        // then the state is the last routine's state. (Success for OR was already handled)
        if (this.index>= this.routines.length) {
            this.state = this.currentRoutine.getState();
        } else {
            this.currentRoutine = this.routines[this.index++];
            this.currentRoutine.start();
        }
    }
}
 function Wait(times) {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.times = 1;
    if (times && times>0) {
        this.times = times;
    }

    this.originalTimes = this.times;

    this.reset = function() {
        this.times = this.originalTimes;
    }

    this.act = function(entity) {
        if (this.times > 0) {
            this.times--;
        }
        if (this.times == 0) {
            this.succeed();
        }
    }
}
 function Wander() {

    var r = new Routine();
    for (var i in r) {
        this[i] = r[i];
    }

    this.random = function() {return ~~(Math.random()*100)};

    this.start = function() {
        this.state = RoutineState.Running;
        this.moveTo.start();
    }

    this.reset = function() {
        this.moveTo = new MoveTo(this.random(), this.random());
    }
    this.reset();

    this.act = function(entity) {
        if (!this.moveTo.isRunning()) {
            return;
        }
        this.moveTo.act(entity);
        if (this.moveTo.isSuccess()) {
            this.succeed();
        } else if (this.moveTo.isFailure()) {
            this.fail();
        }
    }
}
