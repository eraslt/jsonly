var CellularAutomata = {
    
    chanceToStartAlive : 0.2,
    deathLimit : 2,
    birthLimit : 4,
    random : null,

    initialiseMap: function(map) {
        for(var x=0; x<map.length; x++) {
            for(var y=0; y<map.length; y++) {
                if(CellularAutomata.random() < this.chanceToStartAlive) {
                    map[x][y] = true;
                }
            }
        }
        return map;
    },

    /**
     * Generates boolean map[100][100] using celular algorytm for n iterrations
     * @param int numberOfSteps
     * @return boolean[][]
     */
    generateMap: function(numberOfSteps) {
        var cellmap = new Array(100);
        for (var i = 0; i < 100; i++) {
            cellmap[i] = new Array(100);
        }
        return this.generateMapFromMap(numberOfSteps, cellmap);
    },

    /**
     * Generates boolean map from provided map, using celular algorytm for n iterrations
     * @param int numberOfSteps
     * @param boolean[][] cellmap
     * @return boolean[][]
     */
    generateMapFromMap: function(numberOfSteps, cellmap) {
        //Set up the map with random values
        cellmap = this.initialiseMap(cellmap);
        //And now run the simulation for a set number of steps
        for(var i=0; i<numberOfSteps; i++){
            cellmap = this.doSimulationStep(cellmap);
        }
        return cellmap;
    },

    doSimulationStep: function(oldMap) {
        var newMap = new Array(oldMap.length);
        for (var i = 0; i < oldMap.length; i++) {
            newMap[i] = new Array(oldMap.length);
        }
        //Loop over each row and column of the map
        for(var x=0; x<oldMap.length; x++){
            for(var y=0; y<oldMap[0].length; y++){
                var nbs = this.countAliveNeighbours(oldMap, x, y);
                //The new value is based on our simulation rules
                //First, if a cell is alive but has too few neighbours, kill it.
                if(oldMap[x][y]){
                    if(nbs < this.deathLimit){
                        newMap[x][y] = false;
                    }
                    else{
                        newMap[x][y] = true;
                    }
                } //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                else{
                    if(nbs > this.birthLimit){
                        newMap[x][y] = true;
                    }
                    else{
                        newMap[x][y] = false;
                    }
                }
            }
        }
        return newMap;
    },

    /**
     * Returns the number of cells in a ring around (x,y) that are alive.
     * @param boolean[][] map
     * @param int x
     * @param int y
     * @return int
     */
    countAliveNeighbours: function(map, x, y){
        var count = 0;
        for(var i=-1; i<2; i++){
            for(var j=-1; j<2; j++){
                var neighbour_x = x+i;
                var neighbour_y = y+j;
                //If we're looking at the middle point
                if(i == 0 && j == 0){
                    //Do nothing, we don't want to add ourselves in!
                }
                //In case the index we're looking at it off the edge of the map
                else if(neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= map.length || neighbour_y >= map[0].length){
                    count = count + 1;
                }
                //Otherwise, a normal check of the neighbour
                else if(map[neighbour_x][neighbour_y]){
                    count = count + 1;
                }
            }
        }
        return count;
    }
}
;function Entity(x, y) {
    this.x = x;
    this.y = y;
    this.sprite = 0;

    this.setPos = function(x, y) {
        this.x = x;
        this.y = y;
    },

    this.isAlive = function() {
        return true;
    },

    this.update = function() {
        if (null != this.brain) {
            this.brain.act(this);
        }
    }
    
    this.move = function(x, y) {
        if (WorldMap.getMapSprite(x, y)) {
            return false;
        }
        if (!(tile = WorldMap.getMapTile(x, y)) || tile.tex<0) {
            return false;
        }
        this.x = x;
        this.y = y;
        return true;
    },

    this.moveLeft = function() {
        this.sprite = 3;
        return this.move(this.x - 1, this.y);
    },

    this.moveRight = function() {
        this.sprite = 1;
        return this.move(this.x + 1, this.y);
    },

    this.moveDown = function() {
        this.sprite = 0;
        return this.move(this.x, this.y - 1);
    },

    this.moveUp = function() {
        this.sprite = 2;
        return this.move(this.x, this.y + 1);
    },

    this.save = function() {
        var data_str = [];
        for (var i in this) {
            if (typeof this[i] == "string" ||
                typeof this[i] == "number" ||
                typeof this[i] == "boolean") {
                data_str.push("'"+i+"':"+this[i]);
            }
        }
        return "{"+data_str.join()+"}";
    },

    this.load = function(string) {
        var data;
        try {
            eval("data = " + string);
        } catch (e) {
            console.log(e)
        }
        if (data) {
            for (var i in data) {
                if (typeof data[i] == "string" ||
                    typeof data[i] == "number" ||
                    typeof data[i] == "boolean") {
                    this[i] = data[i];
                }
            }
        }
    }
};

;;var randomLong_x = 0;


function randomLong() {
    if (randomLong_x==0) {
        if (world_seed!=undefined && world_seed>0) {
            console.log('a');
            randomLong_x = world_seed;
        } else {
            console.log('b');
            randomLong_x = Math.random();
        }
    }

    if (randomLong_x<1 && randomLong_x>0) {
        randomLong_x = randomLong_x*Math.pow(10, (randomLong_x+'').length-2);
    }

    var x = randomLong_x;
    x ^= (x << 21);
    x ^= (x >>> 35);
    x ^= (x << 4);
    randomLong_x = x;
    return parseFloat('0.'+Math.abs(x));
}
;
var SomePathFinder = {

    findDistances : function(map, pos) {
        for(var x=0; x<map.length; x++){
            for(var y=0; y<map.length; y++){
                map[x][y] = this.euclideanDistance({x:x, y:y}, pos);
            }
        }
        return map;
    },

    simpleWalker : function(start, end) {
        return simpleWalker(start, end, true);
    },

    simpleWalker : function(start, end, diagonal) {
        var max_dist = this.manhattanDistance(start, end);
        var path = new Array();
        start.weight = this.manhattanDistance(start, end);
        var cnt = 0;
        while (JSON.stringify(start)!=JSON.stringify(end)&&++cnt<=max_dist) {
            list = this.findNeighbors(start, diagonal);
            path.push(start);
            for (var a in list) {
                list[a].weight = this.manhattanDistance(list[a], end);
                if (list[a].weight < start.weight) {
                    start = list[a];
                }
            }
        }
        path.push(end);
        return path;
    },

    manhattanDistance : function(a, b) {
        return Math.floor(Math.abs(a.x - b.x) + Math.abs(a.y - b.y));
    },

    euclideanDistance : function(a, b) {
        return Math.sqr((b.x - a.x) * (b.x - a.x) + (b.y - a.y) *(b.y - a.y)); 
    },

    findNeighbors : function(node) {
        return findNeighbors(node, true);
    },

    findNeighbors : function(node, diagonal) {
        list = new Array();
        for(var i=-1; i<2; i++){
            for(var j=-1; j<2; j++){
                if(i == 0 && j == 0) continue;
                if (!diagonal && Math.abs(i+j)!=1) continue;
                list.push({x:node.x + i, y:node.y + j});
            }
        }
        return list;
    },

    test : function() {
        var w = 100;
        var path = SomePathFinder.simpleWalker(
                {x:Math.floor((Math.random() * w) + 1), y:Math.floor((Math.random() * w) + 1)},
                {x:Math.floor((Math.random() * w) + 1), y:Math.floor((Math.random() * w) + 1)}
        );
        console.log(path);
    }
};
;var Sprites = {
    d : [],

    draw : function(ctx, x, y, group, index) {

        if (typeof(Sprites.d[group])=="undefined") {
            return false;
        }

        var sprite = Sprites.d[group];

        if (index>sprite.totalTiles) {
            return false;
        }

        var j = Math.floor(index/sprite.tilesX);
        var i = Math.floor(index - j*sprite.tilesX);
        //TODO kai didels drawint i wirsu gi
        ctx.drawImage(sprite.img, i*sprite.tileWidth, j*sprite.tileHeight, sprite.tileWidth, sprite.tileHeight, x, y-sprite.tileHeight+32, sprite.tileWidth, sprite.tileHeight);
        return true;
    },

    load : function(src, group, tileWidth, tileHeight) {
        var image = new Image();
            image.src = src;
            image.onload = function() {
                Sprites.create(image, group, tileWidth, tileHeight);
            };
    },

    create : function(image, group, tileWidth, tileHeight) {

        var tilesX = image.width / tileWidth;
        var tilesY = image.height / tileHeight;
        var totalTiles = tilesX * tilesY;

        Sprites.d[group] = {
                img : image,
                tileWidth : tileWidth,
                tileHeight : tileHeight,
                tilesX : tilesX,
                tilesY : tilesY,
                totalTiles : totalTiles
        };
        return totalTiles;
    }
};
;/*
 * where all numbers are integers, a and b are big primes so that the integer multiplications overflow, 
 * and c and d are some random integers. ^ is bitwise exclusive or. The result is a random integer which
 * you can scale to the desired range.
 */
function getSeed(x, z) {
    var a = 1300309;
    var b = 8269;
    var c = 15;
    var d = 1789;
    return (a * x + b * z + c) ^ d
}

function getRandomInt(min, max, random) {
    return Math.floor(random * (max - min + 1)) + min;;
}

var WorldMap  = {
    mapTitles : null,
    generate: function(w, road_count, random) {
        this.mapTitles = new Array(w);
        for (var i = 0; i < w; i++) {
            this.mapTitles[i] = new Array(w);
        }

        var wmap = new Array(w);
        for (var i = 0; i < w; i++) {
            wmap[i] = new Array(w);
        }

        CellularAutomata.random = random;
        var heightmap = this.getHeightMap(CellularAutomata.generateMapFromMap(5, wmap));

        for(var y=w-1; y>=0; y--) {
            for(var x=0; x<w; x++) {
                var m =  {
                    is_road : false,
                    dir : null,
                    texture : 1,
                    blocks : 0,
                    decoration : null,
                    sprite : null
                };

                if (heightmap[x][y]>0) {
                    m.blocks = heightmap[x][y];
                    m.texture = 0;
                }

                if (random()<0.3) {
                    m.decoration = {tex : 0, ox : 0, oy : 0};
                    m.decoration.tex = getRandomInt(0, 14, random());
                    m.decoration.ox = getRandomInt(0, 24, random());
                    m.decoration.oy = getRandomInt(0, 24, random());
                }

                if (random()<0.2) {
                    m.sprite = getRandomInt(0, 1, random());
                }

                this.mapTitles[x][y] = m;
            }
        }
    },

    getHeightMap: function(w) {
        var map = new Array(w.length);
        for (var i = 0; i < w.length; i++) {
            map[i] = new Array(w.length);
        }
        for(var x=0; x<w.length; x++) {
            for(var y=0; y<w.length; y++) {
                if (w[x][y]) {
                    map[x][y] = 0;
                } else {
                    for(var i=-1; i<2; i++){
                        for(var j=-1; j<2; j++){
                            if(i == 0 && j == 0) continue;
                            if (Math.abs(i+j)!=1) continue;
                            if (x + i < 0 || y + j < 0) continue;
                            if (x + i > w.length-1 || y + j > w.length-1) continue;
                            if (map[x][y] == null) {
                                map[x][y] = 0;
                            }
                            map[x][y] += w[x+i][y+j]?0:1;
                        }
                    }
                }
            }
        }
        return map;
    },

    getCollisionMap: function() {
        var len = this.mapTitles.length;
        var coll = new Array(len);
        for(var x = 0; x < len; x++) {
            coll[x] = new Array(len);
            for(var y = 0; y < len; y++) {
                coll[x][y] = 1;
                if (this.mapTitles[x] && this.mapTitles[x][y]) {
                    if (this.mapTitles[x][y].sprite) {
                        coll[x][y] = 0;
                    }
                }
            }
        }
        return coll;
    },

    getMapTile: function(x, y) {
        if (!this.mapTitles[x] || !this.mapTitles[x][y]) {
            return false;
        }
        return this.mapTitles[x][y];
    },

    getMapDecoration : function (x, y) {
        if (!this.mapTitles[x]) return false;
        if (!this.mapTitles[x][y]) return false;
        if (!this.mapTitles[x][y].decoration) return false;
        return this.mapTitles[x][y].decoration;
    },
    
    getMapSprite : function (x, y) {
        if (!this.mapTitles[x]) return false;
        if (!this.mapTitles[x][y]) return false;
        if (!this.mapTitles[x][y].sprite) return false;
        return this.mapTitles[x][y].sprite;
    }
}
