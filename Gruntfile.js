module.exports = function(grunt) {

grunt.initConfig({
  concat: {
      options: {
          separator: ';',
      },
      main: {
          src: ['js/*.js', '!js/init.js'],
          dest: 'built.js'
      },
      ai: {
          options: {
              separator: ' ',
          },
          src: ['js/ai/*.js'],
          dest: 'ai.js'
      }
  }
});

grunt.loadNpmTasks('grunt-contrib-concat');

grunt.registerTask('default', ['concat:main', 'concat:ai']);
grunt.registerTask('concat_ai', ['concat:ai']);

};
